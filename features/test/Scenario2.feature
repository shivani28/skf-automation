Feature: SKF application Test Scenario 2

  Scenario: Launch SKF application and verify the drop down options test

    When I navigate to application url
    And Click on Accept & continue if displayed
    And Click on Single bearing image
    And select Deep groove ball bearings to bearing type dropdown
    And Type 6203 in Search designation input box
    And Wait for table to load
    And Select the row showing 6203 under Designation header
    And Verify Next button color has turned to Dark grey
