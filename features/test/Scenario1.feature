Feature: SKF application Test Scenario 1

  Scenario: Launch SKF application and verify the drop down options test

    When I navigate to application url
    And Click on Accept & continue if displayed
    And Click on Single bearing image
    And Click on Select bearing type dropdown
    Then Verify the following options are present

    |Insert bearing (Y-bearing)        |
    |Angular contact ball bearings     |
    |Self-aligning ball bearings       |
    |Cylindrical roller bearings       |
    |Needle roller bearings            |
    |Tapered roller bearings           |
    |Spherical roller bearings         |
    |CARB toroidal roller bearings     |
    |Thrust ball bearings              |
    |Cylindrical roller thrust bearings|
    |Needle roller thrust bearings     |
    |Spherical roller thrust bearings  |
    |Track roller                      |
    |Deep groove ball bearings         |

    And Close dropdown without selecting any option



